# filter_bay
A small filter library for BAYesian filtering. It does not try to be an 
"Eierlegende Wollmilchsau" as it accepts that the filter models are just 
too different.
  
# install
Dependencies can be installed via rosdep:
* Eigen3
* GTest

Note that GTest only installs the sources under Ubuntu, execute:
```sh
sudo apt install libgtest-dev build-essential cmake
cd /usr/src/googletest
sudo cmake .
sudo cmake --build . --target install
```

# usage
The classes are all templated. While the motivation for the Kalman filter is to
verify the model dimensions at compile time, the particle filter can actually
use generic transition and observation models.

The typical workflow is to define the generic filter and then retrieving the
concrete model types from it. 
See the test classes on how to create a filter instance.

# CI
A gitlab CI pipeline is implemented, which is based on [ros_gitlab_ci](https://gitlab.com/VictorLamoine/ros_gitlab_ci). The CI is based on the ros:melodic-ros-core docker image and will install the dependencies via rosdep. The gitlab-runner must use a docker executor.