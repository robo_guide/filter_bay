#pragma once
#include <algorithm>
#include <cmath>
#include <filter_bay/utility/log_arithmetics.hpp>
#include <filter_bay/utility/uniform_sampler.hpp>
#include <functional>
#include <vector>

namespace filter_bay {
/**
 * Particle filter which operates in logarithmic probability domain, see:
 * Christian Gentner, Siwei Zhang, and Thomas Jost, “Log-PF: Particle Filtering
 * in Logarithm Domain,” Journal of Electrical and Computer Engineering, vol.
 * 2018, Article ID 5763461, 11 pages, 2018.
 * https://doi.org/10.1155/2018/5763461.
 *
 * T systematic resampling uses the log_exp_sum larger sums instead of the
 * iterative jacobian logarithms.
 */
template <typename StateType>
class LogParticleFilter {
 public:
  /**
   * Create a logarithmic particle with the given number of particles.
   * @param particle_count the number of particles to initialize
   */
  LogParticleFilter(size_t particle_count)
      : log_weights(particle_count),
        states(particle_count),
        particle_count(particle_count) {}

  /**
   * Set the initial state belief. Must have the size particle_count.
   * Distributes the weights uniformly
   * @param inital_states possible initial states
   */
  void initialize(std::vector<StateType> initial_states) {
    if (initial_states.size() == particle_count) {
      states = std::move(initial_states);
      // log(1/belief_size) = -log(belief_size)
      double log_avg = -log(particle_count);
      for (double &current : log_weights) {
        current = log_avg;
      }
    }
  }

  /**
   * Set the priors of the filter step.
   * @param predictions prior states
   */
  void set_predictions(std::vector<StateType> predictions) {
    states = std::move(predictions);
  }

  /**
   * Updates the particle weights by incorporating the observation as a batch.
   * The update step is done in a SIR bootstrap filter fashion. Resampling is
   * performed with the low variance resampling method.
   * @param log_likelihoods the logarithmic likelihoods for all the states (in
   * the same order as these states)
   */
  void update(const std::vector<double> &log_likelihoods) {
    double max_weight = -std::numeric_limits<double>::infinity();
    max_log_like = -std::numeric_limits<double>::infinity();
    // weights as posterior of observation, prior is proposal density
    for (size_t i = 0; i < particle_count; i++) {
      // log(weight*likelihood) = log(weight) + log_likelihood
      log_weights[i] += log_likelihoods[i];
      if (log_likelihoods[i] > max_log_like) {
        max_log_like = log_likelihoods[i];
      }
      // check for MAP here, after resampling the weights are all equal
      if (log_weights[i] > max_weight) {
        map_state = states[i];
        max_weight = log_weights[i];
      }
    }
    // Normalize weights
    log_weights = normalized_logs(log_weights);
    // Perform resampling? Effective sample_size < half of particle count
    double log_resample_threshold = std::log(particle_count / 2.0);
    log_n_eff = log_effective_sample_size(log_weights);
    if (log_n_eff < log_resample_threshold) {
      resample();
    }
  }

  /**
   * Returns the maximum-a-posteriori state from the last update step.
   */
  StateType get_map_state() const { return map_state; }

  /**
   * Returns the effective sample size. This is calculated before resampling!
   */
  double effective_sample_size() const { return std::exp(log_n_eff); }

  /**
   * Returns maximum log-likelihood from the last update step
   */
  double max_log_likelihood() const { return max_log_like; }

  /**
   * Returns the logarithmic effective sample size from the weights
   * @param log_weight_vec logarithmic weights
   */
  static double log_effective_sample_size(
      const std::vector<double> &log_weight_vec) {
    return log_effective_samples_impl(log_weight_vec);
  }

  /**
   * Returns the state of the particles. Use get_log_weights to obtain the full
   * belief.
   */
  const std::vector<StateType> &get_states() const { return states; }

  /**
   * Returns the logarithmic weights of the particles. Use get_states to obtain
   * the full belief.
   */
  const std::vector<double> &get_log_weights() const { return log_weights; }

  /**
   * Calculates the weights from the logarithmic weights. Use get_states to
   * obtain the full belief.
   */
  std::vector<double> get_weights() const {
    std::vector<double> weights;
    weights.reserve(log_weights.size());
    for (double value : log_weights) {
      weights.push_back(std::exp(value));
    }
    return weights;
  }

  /**
   * Returns the number of particles beeing simulated
   */
  size_t get_particle_count() const { return particle_count; }

  /**
   * Systematic resampling of the particles.
   */
  void resample() {
    // Make copy of old belief
    std::vector<StateType> old_states = states;
    std::vector<double> old_weights = log_weights;
    // Start at random value within average weight
    double cumulative = old_weights[0];
    double avg_weight = 1.0 / particle_count;
    double start_weight = uniform_sampler(0, avg_weight);
    double log_avg = log(avg_weight);
    // o in old_weights, n in new_weights
    size_t o = 0;
    for (size_t n = 0; n < particle_count; n++) {
      double U = log(start_weight + n * avg_weight);
      while (U > cumulative) {
        o++;
        cumulative = jacobi_logarithm(cumulative, old_weights[o]);
      }
      // Resample this particle and reset the weight
      states[n] = old_states[o];
      log_weights[n] = log_avg;
    }
  }

  /**
   * Set the number of simulated particles. The particles are resized and it
   * is advised to call initialize after setting the particle count.
   */
  void set_particle_count(size_t count) {
    particle_count = count;
    states.resize(particle_count);
    log_weights.resize(particle_count);
  }

 private:
  // corrsponding states and weights
  std::vector<double> log_weights;
  std::vector<StateType> states;
  // parameters
  size_t particle_count;
  // maximum-a-posteriori
  StateType map_state;
  double max_log_like;
  double log_n_eff;
  // Uniform random number generator
  UniformSampler uniform_sampler;
};  // namespace filter_bay
}  // namespace filter_bay
