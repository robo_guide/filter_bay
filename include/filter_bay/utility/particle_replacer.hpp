#include <algorithm>
#include <filter_bay/utility/uniform_sampler.hpp>
#include <vector>

namespace filter_bay {

class ParticleReplacer {
 private:
  std::mt19937 generator;
  UniformSampler uniform_sampler;

 public:
  /**
   * Create the replacer with a true random seed
   */
  ParticleReplacer() {
    std::random_device random_device;
    generator = std::mt19937(random_device());
    uniform_sampler = UniformSampler(generator);
  }
  /**
   * Create the replacer with a custom random seed, intended for testing.
   * @param seed the custom random seed
   */
  ParticleReplacer(unsigned int seed)
      : generator(seed), uniform_sampler(generator) {}

  /**
   * Randomly uses either an old or a new state at each position
   * The states must have the equal weights so execute this function after
   * resampling.
   * @param old_states will be partially replaced by new states
   * @param new_states will be partially used to replace old states
   * @param ratio the amount of new states in the result
   */
  template <typename StateType>
  std::vector<StateType> replace(std::vector<StateType> old_states,
                                 const std::vector<StateType>& new_states,
                                 double ratio) {
    for (size_t i = 0; i < old_states.size() && i < new_states.size(); i++) {
      old_states[i] = select(old_states[i], new_states[i], ratio);
    }
    return old_states;
  }

  /**
   * Randomly uses either an old or a new state at each position
   * The states must have the equal weights so execute this function after
   * resampling.
   * @param old_states will be partially replaced by new states
   * @param new_state will be partially used to replace old states
   * @param ratio the amount of new states in the result
   */
  template <typename StateType>
  std::vector<StateType> replace(std::vector<StateType> old_states,
                                 const StateType& new_state, double ratio) {
    for (size_t i = 0; i < old_states.size(); i++) {
      old_states[i] = select(old_states[i], new_state, ratio);
    }
    return old_states;
  }

  /**
   * Returns the new state with a probability of ratio
   * @param old_state selected with (probability - 1)
   * @param new_state selected with probability
   * @param probability element of [0;1]
   */
  template <typename StateType>
  StateType select(StateType old_state, StateType new_state,
                   double probability) {
    auto val = uniform_sampler(0, 1);
    if (val < probability) {
      return new_state;
    } else {
      return old_state;
    }
  }
};
}  // namespace filter_bay
